package edu.uoc.epcsd.showcatalog;

import edu.uoc.epcsd.showcatalog.application.rest.CatalogRESTController;
import edu.uoc.epcsd.showcatalog.domain.Category;
import edu.uoc.epcsd.showcatalog.domain.service.CatalogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CatalogRESTController.class)
public class CatalogControllerUnitTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private CatalogService catalogService;

    @Test
    public void givenCategories_whenFindAllCategories_thenReturnJsonArray() throws Exception {

        Category filmCategory = new Category();
        filmCategory.setName("Films");
        Category musicCategory = new Category();
        filmCategory.setName("Music");

        List<Category> categories = Arrays.asList(filmCategory,musicCategory);

        given(catalogService.findAllCategories()).willReturn(categories);

        mvc.perform(get("/categories").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is(filmCategory.getName())))
                .andExpect(jsonPath("$[1].name", is(musicCategory.getName())));
        verify(catalogService, VerificationModeFactory.times(1)).findAllCategories();
        reset(catalogService);
    }
}
