package edu.uoc.epcsd.showcatalog;

import edu.uoc.epcsd.showcatalog.domain.Show;
import edu.uoc.epcsd.showcatalog.domain.repository.ShowRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class CatalogServiceUnitTest {
    @MockBean
    private ShowRepository showRepository;

    @Before
    public void setUp() {
        Optional<Show> showOptional = Optional.of(new Show());
        Mockito.when(showRepository.findShowById(1L)).thenReturn(showOptional);
        Mockito.when(showRepository.findShowById(-1L)).thenThrow(NoSuchElementException.class);
    }


    @Test
    public void givenValidId_whenFindShowById_thenReturnShow(){
        Show show = showRepository.findShowById(1L).get();
        assertThat(show).isNotNull();
    }

    @Test(expected = NoSuchElementException.class)
    public void givenInvalidId_whenFindShowById_thenReturnException(){
        showRepository.findShowById(-1L).get();
    }
}
