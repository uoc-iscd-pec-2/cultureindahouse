package edu.uoc.epcsd.showcatalog;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.springframework.stereotype.Service;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@AnalyzeClasses(packages = "edu.uoc.epcsd.showcatalog",importOptions = ImportOption.DoNotIncludeTests.class)
public class DomainServiceNamingConventionTest {
    @ArchTest
    static final ArchRule domainServiceNamingConventionIsRespected = classes().that()
            .areAnnotatedWith(Service.class)
            .should()
            .resideInAPackage("..domain.service..")
            .andShould().haveNameMatching(".*ServiceImpl");
}
